/*!
 * Start Bootstrap - Agency Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */




// Highlight the top nav as scrolling occurs
$(function() {
  $('body').scrollspy({
    target: '.navbar-fixed-top'
  })
});

// Closes the Responsive Menu on Menu Item Click
$(function() {
  $('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
  });
});


var sfApp={
  // Image popup effect
  contentPopup: function() {
    if($('.magnific-popup-image').length){
      $('.magnific-popup-image').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom',
        image: {
          verticalFit: true,
          titleSrc: function(item) {
            return item.el.attr('title');
          }
        },
      });
    }
  },

  slick: function() {
    $('.news-posts').slick({
      infinite: false,
      //autoplay: true,
      autoplaySpeed: 6000,
      slidesToScroll: 3,
      //dots: true,
      speed: 300,
      slidesToShow: 3,
      //centerMode: true,
      //variableWidth: true,
      //fade: true,
      //cssEase: 'linear',
      prevArrow: $('.prev'),
      nextArrow: $('.next'),
    });
  },

  animateHeader: function() {
    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
      var $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: $($anchor.attr('href')).offset().top
      }, 1500, 'easeInOutExpo');
      event.preventDefault();
    });
  },

  responsivePictureMap: function() {
    $('img[usemap]').rwdImageMaps();
    /*
    $('area').on('mouseover', function() {
      $("#nejakydiv").html($(this).attr('alt'));
    });

    $("a[href=#]").on('click', function(e) {
      e.preventDefault();
    });
    */

    $('area').on('mouseover', function() {
      var name = $(this).attr('alt');

      $('#classroom li').each(function(i, li) {
          var li = $(li);
          if (li.html().search(name) > 0) {
              li.css('color', '#fed136');
          }
      });
    });

    $('area').on('mouseout', function() {
      $('#classroom li').css('color', '');
    });

    //$('area').on('mouseout', function() {
      //$("#nejakydiv").html("");
    //});
  },

  init: function() {
    sfApp.contentPopup();
    sfApp.slick();
    sfApp.animateHeader();
    sfApp.responsivePictureMap();
  }
};

$(document).on('ready page:load', function() {
  "use strict";
  sfApp.init();
});

