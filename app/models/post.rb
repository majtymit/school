class Post < ActiveRecord::Base
  has_many :post_pictures, dependent: :destroy
  accepts_nested_attributes_for :post_pictures, :reject_if => lambda { |attributes| attributes[:picture].blank? }, :allow_destroy => true

  scope :visible, -> { where(visibility: true) }
  scope :featured, -> { where(important: true) }
  scope :newest, -> { order(created_at: :desc) }

  def seo_title
    I18n.transliterate(title).gsub(' ', '-').gsub(/[^a-zA-Z0-9-]/, '')
  end

  def to_param
    "#{id}-#{seo_title}"
  end
end
