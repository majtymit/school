ActiveAdmin.register Post do

  controller do
    def create
      super do |format|
        redirect_to collection_url and return if resource.valid?
      end
    end

    def update
      super do |format|
        redirect_to collection_url and return if resource.valid?
      end
    end
  end

  member_action :copy, method: :get do
    @post = resource.dup
    render :new, layout: false
  end

  permit_params :id, :visibility, :position, :title, :description, :text, :important, :created_at, :updated_at, post_pictures_attributes: [:id, :_destroy, :picture]

  config.sort_order = 'created_at_desc'
  config.per_page = 15
  filter :title
  filter :text
  filter :title_or_text, as: :string
  filter :created_at

  index do
    selectable_column
      column "ID", :id
      column "Zverejnené", :visibility, sortable: :visibility do |post|
        if post.visibility
          link_to image_tag("yes.png", height: "50"), edit_admin_post_path(post)
        else
          link_to image_tag("no.png", height: "50"), edit_admin_post_path(post)
        end
      end
      column "pozícia", sortable: :position
      column "Názov", sortable: :title, :class => "aa-text_and_name_column" do |post|
        post.title
      end
      column "Text", sortable: :text, :class => "aa-text_and_name_column" do |post|
        post.text.truncate(60, separator: /\s/).html_safe
      end
      column "Dôležité", :important, sortable: :important do |post|
        if post.important
          link_to image_tag("yes.png", height: "25"), edit_admin_post_path(post)
        end
      end
      column "Obrázok" do |post|
        if post.post_pictures.count > 0
          link_to image_tag(post.post_pictures[0].picture.url, height: "50"), edit_admin_post_path(post)
        end
      end
      #Blogpictures.each do |blogpicture|
        #link_to image_tag(blogpictures[0].picture.url, height: "50")
      #end
      column "Vytvorené", sortable: :created_at do |post|
        post.created_at.localtime.strftime("%d.%m.%Y<br />%H:%M:%S").html_safe
      end
      actions defaults: true do |post|
        link_to "Copy", copy_admin_post_path(post)
      end
  end

  form do |f|
    f.inputs do
      f.input :visibility, label: "Zverejnený"
      f.input :position, label: "Pozícia"
      f.input :title, required: true, label: "Názov"
      f.input :description, required: true, label: "Krátky popis"
      f.input :text, required: true, label: "Text"#, as: :rich, config: { height: "300px" }
      f.has_many :post_pictures do |ff|
        ff.input :picture, required: false, as: :file, hint: image_tag(ff.object.picture.url, height: "300")
        ff.input :_destroy, as: :boolean
      end
      f.input :important, required: true, label: "Dôležité"
      f.input :created_at, label: "Vytvorený"
    end
    f.actions
  end

end
