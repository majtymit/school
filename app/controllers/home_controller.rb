class HomeController < ApplicationController
  def index
    @posts = Post.visible.newest
  end

  def show
    @post = Post.find(params[:id])
  end

end
