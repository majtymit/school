Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get '/' => 'home#index', as: :home

  scope 'triedna-kniha' do
    get '/' => 'classbook#index', as: :classbook
  end

  get ':id' => 'home#show', as: :post

end
