class CreatePostPictures < ActiveRecord::Migration
  def change
    create_table :post_pictures do |t|
      t.attachment :picture
      t.belongs_to :post

      t.timestamps null: false
    end
  end
end
