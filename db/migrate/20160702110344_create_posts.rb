class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.boolean :visibility
      t.integer :position
      t.string :title
      t.string :description
      t.text :text
      t.boolean :important
      #t.integer :visits, default: 0

      t.timestamps null: false
    end
  end
end
